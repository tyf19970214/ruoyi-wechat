package com.ruoyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * 启动程序
 * 
 * @author ruoyi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class ApplicationRun
{
    public static void main(String[] args)
    {
        ConfigurableApplicationContext context = SpringApplication.run(ApplicationRun.class, args);
        System.out.println(transColor("(♥◠‿◠)ﾉﾞ  启动成功   ლ(´ڡ`ლ)ﾞ"));
        System.out.println(transColor(getProperties(context.getEnvironment())));
    }

    private static String getProperties(Environment environment) {
        String appName=environment.getProperty("spring.application.name");
        String active=environment.getProperty("spring.profiles.active");
        String server=environment.getProperty("server.port");
        String contextPath=environment.getProperty("server.servlet.context-path");

        StringBuffer sb=new StringBuffer();
        sb.append("activeProfiles=").append(active).append(",");
        sb.append("appName=").append(appName).append(",");
        sb.append("serverPort=").append(server).append(",");
        sb.append("contextPath=").append(contextPath).append(";");

        return sb.toString();
    }

    private static String transColor(String str){
        return AnsiOutput.toString(AnsiColor.BRIGHT_YELLOW,str);
    }
}
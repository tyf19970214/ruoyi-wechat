package com.ruoyi.system.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author TYF http://blog.dwt1997.com
 * @ClassName:
 * @Description: (这里用一句话描述这个类的作用)
 * @date
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccessToken {

    private String tokenName; //获取到的凭证
    private int expireSecond;    //凭证有效时间  单位:秒
}

package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.DataSource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 员工信息对象 t_employee_info
 *
 * @author TYF
 * @date 2022-07-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TEmployeeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 员工编号 */
    private String userId;

    /** 岗位编码 */
    private Long postCode;

    /** 账号 */
    private String loginName;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 组织编码 */
    private Long deptCode;

    /** 类型（00系统用户 01注册用户） */
    private String userType;

    /** 保险编号 */
    private String insuranceNum;

    /** 用户性别（0男 1女 2未知） */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 民族 */
    @Excel(name = "民族")
    private String nation;

    /** 合同时间 */
    private Date contractTime;

    /** 终止时间 */
    private Date stopTime;

    /** 进厂时间 */
    private Date entryTime;

    /** 工龄 */
    private Long workAge;

    /** 出生时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthTime;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 工作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "工作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date wokeTime;

    /** 第一学历 */
    @Excel(name = "第一学历")
    private String firstDegree;

    /** 最高学历 */
    @Excel(name = "最高学历")
    private String highestDegree;

    /** 岗位编码 */
    private Long postId;

    /** 身份编号 */
    private String identityNum;

    /** 身份证号码 */
    @Excel(name = "身份证号码")
    private String identityCardNum;

    /** 职称序列 */
    private String titleSeq;

    /** 职称级别 */
    private String titleLevel;

    /** 职称名称 */
    private String titleName;

    /** 毕业院校 */
    @Excel(name = "毕业院校")
    private String graduateSchool;

    /** 毕业专业 */
    @Excel(name = "毕业专业")
    private String graduationMajor;

    /** 毕业院校 */
    @Excel(name = "最高学历毕业院校")
    private String school;

    /** 毕业专业 */
    @Excel(name = "最高学历毕业专业")
    private String major;


    /** 电话 */
    @Excel(name = "电话")
    private String telephone;

    /** 家庭住址 */
    @Excel(name = "家庭住址")
    private String address;

    /** 是否党员 */
    @Excel(name = "是否党员")
    private String isParty;

    /** 婚否 */
    @Excel(name = "婚否")
    private String isMarriage;

    /** 紧急联系人姓名 */
    @Excel(name = "紧急联系人姓名")
    private String emergencyName;

    /** 紧急联系电话 */
    @Excel(name = "紧急联系电话")
    private String emergencyTelephone;

    /** 原工作单位名称 */
    @Excel(name = "原工作单位名称")
    private String beforeCorporate;

    /** 原工作岗位 */
    @Excel(name = "原工作岗位")
    private String beforePost;

    /** 预留字段1 */
    private String columnOne;

    /** 预留字段2 */
    private String columnTwo;

    /** 预留字段4 */
    private Long columnFour;

    /** 头像路径 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 预留字段三 */
    private Long columnThree;

    /** 盐加密 */
    private String salt;

    /** 帐号状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    private String loginIp;

    /** 最后登录时间 */
    private Date loginDate;

    /** 密码最后更新时间 */
    private Date pwdUpdateDate;

    /** 微信openid */
    private String openid;

    /** 身份证照片 */
    @Excel(name = "身份证照片")
    private String f1;

    /** 学历证明 */
    @Excel(name = "学历证明")
    private String f2;

    /** 个人证件照 */
    @Excel(name = "个人证件照")
    private String f3;

    /** 工资银行卡号 */
    @Excel(name = "工资银行卡号")
    private String f4;

    /** 职业资格证书 */
    @Excel(name = "职业资格证书")
    private String f5;

    /** 体检报告 */
    @Excel(name = "体检报告")
    private String f6;

    /** 户口本（本人和户主） */
    @Excel(name = "户口本", readConverterExp = "本=人和户主")
    private String f7;

    /** 其他附件 */
    @Excel(name = "其他附件")
    private String f8;

    private String accessToken;

    private String approvalStatus;//审批状态

    private String approvalIdea;//拒绝原因
}

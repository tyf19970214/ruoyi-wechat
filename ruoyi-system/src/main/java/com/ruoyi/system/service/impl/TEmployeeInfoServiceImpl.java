package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TEmployeeInfoMapper;
import com.ruoyi.system.domain.TEmployeeInfo;
import com.ruoyi.system.service.ITEmployeeInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 员工信息Service业务层处理
 *
 * @author TYF
 * @date 2022-07-29
 */
@Service
public class TEmployeeInfoServiceImpl implements ITEmployeeInfoService 
{
    @Autowired
    private TEmployeeInfoMapper tEmployeeInfoMapper;

    /**
     * 查询员工信息
     * 
     * @param id 员工信息ID
     * @return 员工信息
     */
    @Override
    public TEmployeeInfo selectTEmployeeInfoById(Long id)
    {
        return tEmployeeInfoMapper.selectTEmployeeInfoById(id);
    }


    @Override
    public TEmployeeInfo selectTEmployeeInfoByOpenId(String openId) {
        return tEmployeeInfoMapper.selectTEmployeeInfoByOpenId(openId);
    }

    /**
     * 查询员工信息列表
     * 
     * @param tEmployeeInfo 员工信息
     * @return 员工信息
     */
    @Override
    public List<TEmployeeInfo> selectTEmployeeInfoList(TEmployeeInfo tEmployeeInfo)
    {
        return tEmployeeInfoMapper.selectTEmployeeInfoList(tEmployeeInfo);
    }

    /**
     * 新增员工信息
     * 
     * @param tEmployeeInfo 员工信息
     * @return 结果
     */
    @Override
    public int insertTEmployeeInfo(TEmployeeInfo tEmployeeInfo)
    {
        tEmployeeInfo.setCreateTime(DateUtils.getNowDate());
        return tEmployeeInfoMapper.insertTEmployeeInfo(tEmployeeInfo);
    }

    /**
     * 修改员工信息
     * 
     * @param tEmployeeInfo 员工信息
     * @return 结果
     */
    @Override
    public int updateTEmployeeInfo(TEmployeeInfo tEmployeeInfo)
    {
        tEmployeeInfo.setUpdateTime(DateUtils.getNowDate());
        return tEmployeeInfoMapper.updateTEmployeeInfo(tEmployeeInfo);
    }

    /**
     * 删除员工信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTEmployeeInfoByIds(String ids)
    {
        return tEmployeeInfoMapper.deleteTEmployeeInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除员工信息信息
     * 
     * @param id 员工信息ID
     * @return 结果
     */
    @Override
    public int deleteTEmployeeInfoById(Long id)
    {
        return tEmployeeInfoMapper.deleteTEmployeeInfoById(id);
    }
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TEmployeeInfo;

/**
 * 员工信息Service接口
 *
 * @author TYF
 * @date 2022-07-29
 */
public interface ITEmployeeInfoService 
{
    /**
     * 查询员工信息
     * 
     * @param id 员工信息ID
     * @return 员工信息
     */
    public TEmployeeInfo selectTEmployeeInfoById(Long id);


    /**
     * 查询员工信息
     *
     * @param openId 员工微信ID
     * @return 员工信息
     */
    public TEmployeeInfo selectTEmployeeInfoByOpenId(String openId);

    /**
     * 查询员工信息列表
     * 
     * @param tEmployeeInfo 员工信息
     * @return 员工信息集合
     */
    public List<TEmployeeInfo> selectTEmployeeInfoList(TEmployeeInfo tEmployeeInfo);

    /**
     * 新增员工信息
     * 
     * @param tEmployeeInfo 员工信息
     * @return 结果
     */
    public int insertTEmployeeInfo(TEmployeeInfo tEmployeeInfo);

    /**
     * 修改员工信息
     * 
     * @param tEmployeeInfo 员工信息
     * @return 结果
     */
    public int updateTEmployeeInfo(TEmployeeInfo tEmployeeInfo);

    /**
     * 批量删除员工信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTEmployeeInfoByIds(String ids);

    /**
     * 删除员工信息信息
     * 
     * @param id 员工信息ID
     * @return 结果
     */
    public int deleteTEmployeeInfoById(Long id);
}

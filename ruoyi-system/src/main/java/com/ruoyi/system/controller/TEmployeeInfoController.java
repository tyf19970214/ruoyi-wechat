package com.ruoyi.system.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.service.ISysNoticeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TEmployeeInfo;
import com.ruoyi.system.service.ITEmployeeInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 员工信息Controller
 *
 * @author TYF
 * @date 2022-07-29
 */
@Controller
@RequestMapping("/system/info")
public class TEmployeeInfoController extends BaseController
{

    Logger logger =LoggerFactory.getLogger(TEmployeeInfoController.class);


    private String prefix = "system/info";

    @Autowired
    private ITEmployeeInfoService tEmployeeInfoService;

    @Autowired
     private ISysNoticeService noticeService;

    String[] allowedFileExtensions = {"jpg","png", "jpeg", "gif", "pdf"};

    @RequiresPermissions("system:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }

    /**
     * 查询员工信息列表
     */
    @RequiresPermissions("system:info:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TEmployeeInfo tEmployeeInfo)
    {
        startPage();
        List<TEmployeeInfo> list = tEmployeeInfoService.selectTEmployeeInfoList(tEmployeeInfo);
        return getDataTable(list);
    }

    /**
     * 导出员工信息列表
     */
    @RequiresPermissions("system:info:export")
    @Log(title = "员工信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TEmployeeInfo tEmployeeInfo)
    {
        List<TEmployeeInfo> list = tEmployeeInfoService.selectTEmployeeInfoList(tEmployeeInfo);
        ExcelUtil<TEmployeeInfo> util = new ExcelUtil<TEmployeeInfo>(TEmployeeInfo.class);
        return util.exportExcel(list, "员工信息数据");
    }

    /**
     * 新增员工信息
     */
    @GetMapping(value = {"/add"})
    public String add(String openId, ModelMap mmap)
    {
        TEmployeeInfo tEmployeeInfo = tEmployeeInfoService.selectTEmployeeInfoByOpenId(openId);
        mmap.put("tEmployeeInfo", tEmployeeInfo);
        mmap.put("openId",openId);
        List<SysNotice> sysNotices = noticeService.selectNoticeByUser(openId);
        if(sysNotices.size()>0){
            mmap.put("sysNotices", sysNotices);
        }else{
            mmap.put("error", "暂无数据");
        }

        return prefix + "/add";
    }




    /**
     * 新增保存员工信息
     */
    /*@RequiresPermissions("system:info:add")*/
    @Log(title = "员工信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam("files") MultipartFile[] files, TEmployeeInfo tEmployeeInfo) {

        //上传路径
        String filePath = null;
        // 上传并返回新文件名称
        String fileName = null;
        for(int i=0;i<files.length;i++){
            MultipartFile file=files[i];
            try {
                filePath = RuoYiConfig.getUploadPath()+"/"+tEmployeeInfo.getIdentityCardNum();
                fileName = FileUploadUtils.uploadContract(filePath,file.getOriginalFilename(),file,null);

                if(i == 0) {
                    tEmployeeInfo.setF1(filePath+fileName);
                }else if(i == 1) {
                    tEmployeeInfo.setF2(filePath+fileName);
                }else if(i == 2) {
                    tEmployeeInfo.setF3(filePath+fileName);
                }else if(i == 3) {
                    tEmployeeInfo.setF4(filePath+fileName);
                }else if(i == 4) {
                    tEmployeeInfo.setF5(filePath+fileName);
                }else if(i == 5) {
                    tEmployeeInfo.setF6(filePath+fileName);
                }else if(i == 6) {
                    tEmployeeInfo.setF7(filePath+fileName);
                }else if(i == 7) {
                    tEmployeeInfo.setF8(filePath+fileName);
                }

            } catch (InvalidExtensionException | IOException e) {
                throw new BusinessException("上传文件失败",e);
            }
        }

        return toAjax(tEmployeeInfoService.insertTEmployeeInfo(tEmployeeInfo));
    }

    /**
     * 修改员工信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TEmployeeInfo tEmployeeInfo = tEmployeeInfoService.selectTEmployeeInfoById(id);
        mmap.put("tEmployeeInfo", tEmployeeInfo);
        return prefix + "/edit";
    }



    /**
     * 修改员工信息
     */
    @GetMapping("/editApprovalIdea/{id}")
    public String editApprovalIdea(@PathVariable("id") Long id, ModelMap mmap)
    {
        TEmployeeInfo tEmployeeInfo = tEmployeeInfoService.selectTEmployeeInfoById(id);
        mmap.put("tEmployeeInfo", tEmployeeInfo);
        return prefix + "/editApprovalIdea";
    }


    /**
     * 修改员工信息
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap)
    {
        TEmployeeInfo tEmployeeInfo = tEmployeeInfoService.selectTEmployeeInfoById(id);
        mmap.put("tEmployeeInfo", tEmployeeInfo);
        return prefix + "/detail";
    }


    /**
     * 修改保存员工信息
     */
    @RequiresPermissions("system:info:edit")
    @Log(title = "员工信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestParam("files") MultipartFile[] files,TEmployeeInfo tEmployeeInfo)
    {
        //上传路径
        String filePath = null;
        // 上传并返回新文件名称
        String fileName = null;
        for(int i=0;i<files.length;i++){
            MultipartFile file=files[i];
            try {
                filePath = RuoYiConfig.getUploadPath()+"/"+tEmployeeInfo.getIdentityCardNum();
                fileName = FileUploadUtils.uploadContract(filePath,file.getOriginalFilename(),file,null);

                if(i == 0) {
                    tEmployeeInfo.setF1(filePath+fileName);
                }else if(i == 1) {
                    tEmployeeInfo.setF2(filePath+fileName);
                }else if(i == 2) {
                    tEmployeeInfo.setF3(filePath+fileName);
                }else if(i == 3) {
                    tEmployeeInfo.setF4(filePath+fileName);
                }else if(i == 4) {
                    tEmployeeInfo.setF5(filePath+fileName);
                }else if(i == 5) {
                    tEmployeeInfo.setF6(filePath+fileName);
                }else if(i == 6) {
                    tEmployeeInfo.setF7(filePath+fileName);
                }else if(i == 7) {
                    tEmployeeInfo.setF8(filePath+fileName);
                }

            } catch (InvalidExtensionException | IOException e) {
                throw new BusinessException("上传文件失败",e);
            }
        }

        return toAjax(tEmployeeInfoService.updateTEmployeeInfo(tEmployeeInfo));
    }

    /**
     * 拒绝原因
     */
    @PostMapping("/editApprovalIdea")
    @ResponseBody
    public AjaxResult editApprovalIdea(TEmployeeInfo tEmployeeInfo)
    {
        String approvalIdea = tEmployeeInfo.getApprovalIdea();
        if(StringUtils.isEmpty(approvalIdea)){

            return AjaxResult.error("原因不可为空");
        }
        TEmployeeInfo employeeInfo = tEmployeeInfoService.selectTEmployeeInfoById(tEmployeeInfo.getId());
        SysNotice sysNotice =new SysNotice();
        sysNotice.setNoticeTitle("拒绝通知").setNoticeType("1").setNoticeContent(employeeInfo.getUserName()+"由于<span style='color:red;'>"+tEmployeeInfo.getApprovalIdea()+"</span>导致审核未通过，请重新填写信息");
        sysNotice.setCreateBy(employeeInfo.getOpenid());
        sysNotice.setCreateTime(new Date());
        int i = noticeService.insertNotice(sysNotice);
        logger.debug("拒绝通知添加成功:"+i);
        return toAjax(tEmployeeInfoService.updateTEmployeeInfo(tEmployeeInfo));
    }

    /**
     * 修改保存员工信息
     */
    @PostMapping("/editSatus/{id}")
    @ResponseBody
    public AjaxResult editSatus(@PathVariable("id") Long id)
    {
        AjaxResult ajaxResult =new AjaxResult();
        TEmployeeInfo employeeInfo = tEmployeeInfoService.selectTEmployeeInfoById(id);
        if(null!=employeeInfo){
            TEmployeeInfo employeeInfoUpdate=new TEmployeeInfo();
            employeeInfoUpdate.setApprovalStatus("1");
            employeeInfoUpdate.setId(employeeInfo.getId());
            int i = tEmployeeInfoService.updateTEmployeeInfo(employeeInfoUpdate);
            if(i>0){
                SysNotice sysNoticeQuery=new SysNotice();
                sysNoticeQuery.setCreateBy(employeeInfo.getOpenid());
                sysNoticeQuery.setCreateTime(new Date());
                sysNoticeQuery.setNoticeTitle("审批通知").setNoticeType("通知").setNoticeContent(employeeInfo.getUserName()+"个人信息审核通过");
                int i1 = noticeService.insertNotice(sysNoticeQuery);
                logger.debug("通知添加成功:"+i1);
                ajaxResult.put(AjaxResult.CODE_TAG,0);
                ajaxResult.put(AjaxResult.MSG_TAG,"审批成功");
                return ajaxResult;
            }else{
                ajaxResult.put(AjaxResult.CODE_TAG,500);
                ajaxResult.put(AjaxResult.MSG_TAG,"审批失败，请查看不合格内容");
                return ajaxResult;
            }
        }
        ajaxResult.put(AjaxResult.CODE_TAG,500);
        ajaxResult.put(AjaxResult.MSG_TAG,"该信息为为空");
        return ajaxResult;
    }

    /**
     * 删除员工信息
     */
    @RequiresPermissions("system:info:remove")
    @Log(title = "员工信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tEmployeeInfoService.deleteTEmployeeInfoByIds(ids));
    }
}

package com.ruoyi.weixin.entity;

/**
 * 菜单 
 *
 * @author TYF
 * @date 2022-07-29
 */  
public class Menu {  
    private Button[] button;

    public Button[] getButton() {
        return button;  
    }  
  
    public void setButton(Button[] button) {
        this.button = button;  
    }  
}  
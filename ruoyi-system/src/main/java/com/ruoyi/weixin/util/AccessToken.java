package com.ruoyi.weixin.util;
/** 
 * 微信通用接口凭证 
 *
 * @author TYF
 * @date 2022-07-29
 */  
public class AccessToken {  
    // 获取到的凭证  
    private String token;  
    // 凭证有效时间，单位：秒  
    private int expiresIn;  
  
    private Long expiresTiem;
    
    public String getToken() {  
        return token;  
    }  
  
    public void setToken(String token) {  
        this.token = token;  
    }  
  
    public int getExpiresIn() {  
        return expiresIn;  
    }  
  
    public void setExpiresIn(int expiresIn) {  
        this.expiresIn = expiresIn;  
    }

	public Long getExpiresTiem() {
		return expiresTiem;
	}

	public void setExpiresTiem(Long expiresTiem) {
		this.expiresTiem = expiresTiem;
	}
	
	/**
	 * 判断token是否过期
	 * @return
	 */
	public boolean isExpired() {
		return System.currentTimeMillis() > expiresTiem;
	}

	@Override
	public String toString() {
		return "AccessToken [token=" + token + ", expiresIn=" + expiresIn + "]";
	}  
}
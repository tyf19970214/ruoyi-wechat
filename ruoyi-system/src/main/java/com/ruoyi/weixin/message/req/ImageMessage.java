package com.ruoyi.weixin.message.req;

/**
 * 图片消息 
 *
 * @author TYF
 * @date 2022-07-29
 */  
public class ImageMessage extends BaseMessage {
    // 图片链接  
    private String PicUrl;  
  
    public String getPicUrl() {  
        return PicUrl;  
    }  
  
    public void setPicUrl(String picUrl) {  
        PicUrl = picUrl;  
    }  
}  
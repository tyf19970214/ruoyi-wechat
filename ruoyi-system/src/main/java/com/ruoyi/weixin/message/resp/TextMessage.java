package com.ruoyi.weixin.message.resp;

/**
 * 文本消息 
 *
 * @author TYF
 * @date 2022-07-29
 */  
public class TextMessage extends BaseMessage {
    // 回复的消息内容  
    private String Content;  
  
    public String getContent() {  
        return Content;  
    }  
  
    public void setContent(String content) {  
        Content = content;  
    }  
}  
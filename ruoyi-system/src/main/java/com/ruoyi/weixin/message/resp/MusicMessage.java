package com.ruoyi.weixin.message.resp;

/**
 * 音乐消息 
 *
 * @author TYF
 * @date 2022-07-29
 */  
public class MusicMessage extends BaseMessage {
    // 音乐  
    private com.ruoyi.weixin.message.resp.Music Music;

    public com.ruoyi.weixin.message.resp.Music getMusic() {
        return Music;
    }

    public void setMusic(com.ruoyi.weixin.message.resp.Music music) {
        Music = music;  
    }  
} 

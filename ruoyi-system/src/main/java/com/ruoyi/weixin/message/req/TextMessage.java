package com.ruoyi.weixin.message.req;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/** 
 * 文本消息 
 *
 * @author TYF
 * @date 2022-07-29
 */  
@XStreamAlias("xml")
public class TextMessage extends BaseMessage {
    // 消息内容  
    private String Content;  
    private Integer funcFlag;
    
    public String getContent() {  
        return Content;  
    }  
  
    public void setContent(String content) {  
        Content = content;  
    }

	public Integer getFuncFlag() {
		return funcFlag;
	}

	public void setFuncFlag(Integer funcFlag) {
		this.funcFlag = funcFlag;
	}

}  
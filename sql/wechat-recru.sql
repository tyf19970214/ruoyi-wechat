/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50728
Source Host           : localhost:3306
Source Database       : wechat-recru

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2022-08-06 19:51:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '初始化密码 123456');
INSERT INTO `sys_config` VALUES ('3', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES ('4', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES ('5', '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config` VALUES ('6', '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES ('7', '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES ('8', '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config` VALUES ('9', '主框架页-是否开启页脚', 'sys.index.ignoreFooter', 'true', 'Y', 'admin', '2021-06-07 18:52:13', '', null, '是否开启底部页脚显示（true显示，false隐藏）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(11) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-06-07 18:52:13', '', null, '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '性别女');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-06-07 18:52:13', '', null, '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-06-07 18:52:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-06-07 18:52:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-06-07 18:52:13', '', null, '默认分组');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '系统分组');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-06-07 18:52:13', '', null, '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-06-07 18:52:13', '', null, '通知');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '公告');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-06-07 18:52:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('18', '99', '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '其他操作');
INSERT INTO `sys_dict_data` VALUES ('19', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '新增操作');
INSERT INTO `sys_dict_data` VALUES ('20', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '修改操作');
INSERT INTO `sys_dict_data` VALUES ('21', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '删除操作');
INSERT INTO `sys_dict_data` VALUES ('22', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '授权操作');
INSERT INTO `sys_dict_data` VALUES ('23', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '导出操作');
INSERT INTO `sys_dict_data` VALUES ('24', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '导入操作');
INSERT INTO `sys_dict_data` VALUES ('25', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '强退操作');
INSERT INTO `sys_dict_data` VALUES ('26', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '生成操作');
INSERT INTO `sys_dict_data` VALUES ('27', '9', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '清空操作');
INSERT INTO `sys_dict_data` VALUES ('28', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('29', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-06-07 18:52:13', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('100', '1', '是', 'Y', 'isParty', null, null, 'Y', '0', 'admin', '2021-06-01 15:05:29', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('101', '2', '否', 'N', 'isParty', null, null, 'Y', '0', 'admin', '2021-06-01 15:05:41', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('102', '1', '婚', 'Y', 'isMarriage', '', '', 'Y', '0', 'admin', '2021-06-01 15:06:20', 'admin', '2021-06-01 15:10:12', '');
INSERT INTO `sys_dict_data` VALUES ('103', '2', '否', 'N', 'isMarriage', null, null, 'Y', '0', 'admin', '2021-06-01 15:06:31', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('104', '1', '一线', '0', 'salaryType', null, null, 'Y', '0', 'admin', '2021-06-04 20:52:58', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('105', '2', '二线', '1', 'salaryType', null, null, 'Y', '0', 'admin', '2021-06-04 20:53:10', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('106', '3', '干部', '2', 'salaryType', null, null, 'Y', '0', 'admin', '2021-06-04 20:53:41', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('107', '4', '年薪金', '3', 'salaryType', null, null, 'Y', '0', 'admin', '2021-06-04 20:53:58', '', null, null);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE KEY `dict_type` (`dict_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2021-06-07 18:52:13', '', null, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2021-06-07 18:52:13', '', null, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2021-06-07 18:52:13', '', null, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2021-06-07 18:52:13', '', null, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', '2021-06-07 18:52:13', '', null, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', '2021-06-07 18:52:13', '', null, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知类型', 'sys_notice_type', '0', 'admin', '2021-06-07 18:52:13', '', null, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', '2021-06-07 18:52:13', '', null, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', '2021-06-07 18:52:13', '', null, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', '2021-06-07 18:52:13', '', null, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('100', '是否党员', 'isParty', '0', 'admin', '2021-06-01 15:05:10', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('107', '婚否', 'isMarriage', '0', 'admin', '2021-06-01 15:06:05', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('108', '工资类型', 'salaryType', '0', 'admin', '2021-06-04 20:52:40', '', null, null);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-06-07 18:52:14', '', null, '');
INSERT INTO `sys_job` VALUES ('2', '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-06-07 18:52:14', '', null, '');
INSERT INTO `sys_job` VALUES ('3', '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-06-07 18:52:14', '', null, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('101', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2021-06-07 19:06:53');
INSERT INTO `sys_logininfor` VALUES ('102', '10135065', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-07 19:07:18');
INSERT INTO `sys_logininfor` VALUES ('103', '13022099', '127.0.0.1', '内网IP', 'Chrome 59', 'Windows 10', '0', '登录成功', '2021-06-07 19:40:13');
INSERT INTO `sys_logininfor` VALUES ('104', '13022099', '127.0.0.1', '内网IP', 'Chrome 59', 'Windows 10', '0', '退出成功', '2021-06-07 19:57:02');
INSERT INTO `sys_logininfor` VALUES ('105', '13022099', '127.0.0.1', '内网IP', 'Chrome 59', 'Windows 10', '0', '登录成功', '2021-06-07 19:57:53');
INSERT INTO `sys_logininfor` VALUES ('106', '13022099', '127.0.0.1', '内网IP', 'Chrome 59', 'Windows 10', '0', '登录成功', '2021-06-07 20:08:02');
INSERT INTO `sys_logininfor` VALUES ('107', '13022099', '113.140.3.6', 'XX XX', 'Firefox 8', 'Windows 10', '0', '登录成功', '2021-06-09 09:16:30');
INSERT INTO `sys_logininfor` VALUES ('108', '47.108.192.49', '113.140.3.6', 'XX XX', 'Firefox 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-06-09 09:32:48');
INSERT INTO `sys_logininfor` VALUES ('109', '13022099', '113.140.3.6', 'XX XX', 'Firefox 8', 'Windows 10', '0', '登录成功', '2021-06-09 09:33:08');
INSERT INTO `sys_logininfor` VALUES ('110', '13022027', '113.140.3.6', 'XX XX', 'Chrome 53', 'Windows 10', '1', '用户不存在/密码错误', '2021-06-09 17:39:05');
INSERT INTO `sys_logininfor` VALUES ('111', '13022027', '113.140.3.6', 'XX XX', 'Chrome 53', 'Windows 10', '1', '密码输入错误1次', '2021-06-09 17:41:36');
INSERT INTO `sys_logininfor` VALUES ('112', '13022028', '124.114.244.123', 'XX XX', 'Chrome Mobile', 'Android 1.x', '1', '密码输入错误1次', '2021-06-09 17:42:20');
INSERT INTO `sys_logininfor` VALUES ('113', '13022028', '124.114.244.123', 'XX XX', 'Chrome Mobile', 'Android 1.x', '1', '用户不存在/密码错误', '2021-06-09 17:42:29');
INSERT INTO `sys_logininfor` VALUES ('114', '13022027', '113.140.3.6', 'XX XX', 'Chrome 53', 'Windows 10', '0', '登录成功', '2021-06-09 17:44:55');
INSERT INTO `sys_logininfor` VALUES ('115', '13022028', '124.114.244.123', 'XX XX', 'Chrome Mobile', 'Android 1.x', '1', '密码输入错误2次', '2021-06-09 17:45:03');
INSERT INTO `sys_logininfor` VALUES ('116', '13022028', '124.114.244.123', 'XX XX', 'Chrome Mobile', 'Android 1.x', '1', '密码输入错误3次', '2021-06-09 17:45:05');
INSERT INTO `sys_logininfor` VALUES ('117', '13022028', '124.114.244.123', 'XX XX', 'Chrome Mobile', 'Android 1.x', '1', '密码输入错误4次', '2021-06-09 17:46:54');
INSERT INTO `sys_logininfor` VALUES ('118', '13022028', '124.114.244.123', 'XX XX', 'Chrome Mobile', 'Android 1.x', '1', '密码输入错误5次', '2021-06-09 17:46:59');
INSERT INTO `sys_logininfor` VALUES ('119', '13022027', '113.140.3.6', 'XX XX', 'Chrome 53', 'Windows 10', '0', '登录成功', '2021-06-09 17:50:30');
INSERT INTO `sys_logininfor` VALUES ('120', '13020548', '113.140.3.6', 'XX XX', 'Chrome 59', 'Windows 10', '0', '登录成功', '2021-06-10 11:27:25');
INSERT INTO `sys_logininfor` VALUES ('121', '13020565', '113.140.3.6', 'XX XX', 'Chrome 59', 'Windows 10', '0', '登录成功', '2021-06-10 11:28:37');
INSERT INTO `sys_logininfor` VALUES ('122', '13023711', '113.140.3.6', 'XX XX', 'Chrome 59', 'Windows 10', '0', '登录成功', '2021-06-10 11:29:45');
INSERT INTO `sys_logininfor` VALUES ('123', '13020634', '113.140.6.198', 'XX XX', 'Firefox 9', 'Windows 10', '0', '登录成功', '2021-08-05 16:36:23');
INSERT INTO `sys_logininfor` VALUES ('124', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-08-01 11:53:23');
INSERT INTO `sys_logininfor` VALUES ('125', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-08-01 11:54:37');
INSERT INTO `sys_logininfor` VALUES ('126', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-01 15:08:38');
INSERT INTO `sys_logininfor` VALUES ('127', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2022-08-01 15:44:21');
INSERT INTO `sys_logininfor` VALUES ('128', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-08-01 15:44:26');
INSERT INTO `sys_logininfor` VALUES ('129', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-08-01 15:57:02');
INSERT INTO `sys_logininfor` VALUES ('130', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-08-01 15:59:50');
INSERT INTO `sys_logininfor` VALUES ('131', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-08-01 16:08:53');
INSERT INTO `sys_logininfor` VALUES ('132', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-08-01 16:37:42');
INSERT INTO `sys_logininfor` VALUES ('133', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-08-01 16:59:25');
INSERT INTO `sys_logininfor` VALUES ('134', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-08-02 09:36:33');
INSERT INTO `sys_logininfor` VALUES ('135', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-08-02 09:55:43');
INSERT INTO `sys_logininfor` VALUES ('136', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-08-02 11:25:22');
INSERT INTO `sys_logininfor` VALUES ('137', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-08-02 11:35:21');
INSERT INTO `sys_logininfor` VALUES ('138', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-08-03 18:12:31');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1077 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '1', '#', '', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2021-06-07 18:52:13', '', null, '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '2', '#', '', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2021-06-07 18:52:13', '', null, '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '3', '#', '', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2021-06-07 18:52:13', '', null, '系统工具目录');
INSERT INTO `sys_menu` VALUES ('4', '若依官网', '0', '4', 'http://ruoyi.vip', 'menuBlank', 'C', '0', '1', '', 'fa fa-location-arrow', 'admin', '2021-06-07 18:52:13', '', null, '若依官网地址');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', '1', '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2021-06-07 18:52:13', '', null, '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1', '2', '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2021-06-07 18:52:13', '', null, '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '1', '3', '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2021-06-07 18:52:13', '', null, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', '4', '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2021-06-07 18:52:13', '', null, '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('104', '岗位管理', '1', '5', '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2021-06-07 18:52:13', '', null, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '字典管理', '1', '6', '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2021-06-07 18:52:13', '', null, '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '参数设置', '1', '7', '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2021-06-07 18:52:13', '', null, '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', '8', '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2021-06-07 18:52:13', '', null, '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '日志管理', '1', '9', '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2021-06-07 18:52:13', '', null, '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '2', '1', '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2021-06-07 18:52:13', '', null, '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('110', '定时任务', '2', '2', '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2021-06-07 18:52:13', '', null, '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('111', '数据监控', '2', '3', '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2021-06-07 18:52:13', '', null, '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('112', '服务监控', '2', '4', '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2021-06-07 18:52:13', '', null, '服务监控菜单');
INSERT INTO `sys_menu` VALUES ('113', '缓存监控', '2', '5', '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2021-06-07 18:52:13', '', null, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES ('114', '表单构建', '3', '1', '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2021-06-07 18:52:13', '', null, '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('115', '代码生成', '3', '2', '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2021-06-07 18:52:13', '', null, '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('116', '系统接口', '3', '3', '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2021-06-07 18:52:13', '', null, '系统接口菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', '1', '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2021-06-07 18:52:13', '', null, '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', '2', '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2021-06-07 18:52:13', '', null, '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1000', '用户查询', '100', '1', '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1001', '用户新增', '100', '2', '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1002', '用户修改', '100', '3', '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1003', '用户删除', '100', '4', '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1004', '用户导出', '100', '5', '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1005', '用户导入', '100', '6', '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1006', '重置密码', '100', '7', '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1007', '角色查询', '101', '1', '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1008', '角色新增', '101', '2', '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1009', '角色修改', '101', '3', '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1010', '角色删除', '101', '4', '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1011', '角色导出', '101', '5', '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1012', '菜单查询', '102', '1', '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单新增', '102', '2', '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单修改', '102', '3', '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1015', '菜单删除', '102', '4', '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1016', '部门查询', '103', '1', '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1017', '部门新增', '103', '2', '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1018', '部门修改', '103', '3', '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1019', '部门删除', '103', '4', '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1020', '岗位查询', '104', '1', '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位新增', '104', '2', '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位修改', '104', '3', '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位删除', '104', '4', '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1024', '岗位导出', '104', '5', '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1025', '字典查询', '105', '1', '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1026', '字典新增', '105', '2', '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1027', '字典修改', '105', '3', '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1028', '字典删除', '105', '4', '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1029', '字典导出', '105', '5', '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1030', '参数查询', '106', '1', '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1031', '参数新增', '106', '2', '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1032', '参数修改', '106', '3', '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1033', '参数删除', '106', '4', '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1034', '参数导出', '106', '5', '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1035', '公告查询', '107', '1', '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1036', '公告新增', '107', '2', '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1037', '公告修改', '107', '3', '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1038', '公告删除', '107', '4', '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1039', '操作查询', '500', '1', '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1040', '操作删除', '500', '2', '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1041', '详细信息', '500', '3', '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1042', '日志导出', '500', '4', '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1043', '登录查询', '501', '1', '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1044', '登录删除', '501', '2', '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1045', '日志导出', '501', '3', '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1046', '账户解锁', '501', '4', '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1047', '在线查询', '109', '1', '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1048', '批量强退', '109', '2', '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1049', '单条强退', '109', '3', '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1050', '任务查询', '110', '1', '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1051', '任务新增', '110', '2', '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1052', '任务修改', '110', '3', '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1053', '任务删除', '110', '4', '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1054', '状态修改', '110', '5', '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1055', '任务详细', '110', '6', '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1056', '任务导出', '110', '7', '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1057', '生成查询', '115', '1', '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1058', '生成修改', '115', '2', '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1059', '生成删除', '115', '3', '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1060', '预览代码', '115', '4', '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1061', '生成代码', '115', '5', '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1063', '人员管理', '1', '10', '/system/info', 'menuItem', 'C', '0', '1', 'system:info:view', '#', 'admin', '2022-08-01 15:48:41', '', null, '');
INSERT INTO `sys_menu` VALUES ('1071', '人员查询', '1063', '1', '#', '', 'F', '0', '1', 'system:info:list', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1072', '人员新增', '1063', '2', '#', '', 'F', '0', '1', 'system:info:add', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1073', '人员修改', '1063', '3', '#', '', 'F', '0', '1', 'system:info:edit', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1074', '人员删除', '1063', '4', '#', '', 'F', '0', '1', 'system:info:remove', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1075', '人员导出', '1063', '5', '#', '', 'F', '0', '1', 'system:info:export', '#', 'admin', '2021-06-07 18:52:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('1076', '人员导入', '1063', '6', '#', '', 'F', '0', '1', 'system:info:import', '#', 'admin', '2021-06-07 18:52:13', '', null, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) DEFAULT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('4', '通知', '1', '党哲个人信息没问题', '0', 'oRx0F1itBjUiRuTCGsX6e3M9nchA', '2022-08-01 17:39:03', 'admin', '2022-08-02 09:48:49', null);
INSERT INTO `sys_notice` VALUES ('5', '拒绝通知', '1', '党哲由于<span style=\'color:red;\'>毕业证 不清晰 重新上传</span>导致审核未通过，请重新填写信息', '0', 'ARx0F1itBjUiRuTCGsX6e3M9nchC', '2022-08-02 11:35:42', '', null, null);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(11) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(11) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(11) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('111', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"1\"],\"deptId\":[\"0\"],\"userName\":[\"若依\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"15888888888\"],\"email\":[\"ry@163.com\"],\"loginName\":[\"admin\"],\"sex\":[\"1\"],\"role\":[\"1\"],\"status\":[\"0\"],\"roleIds\":[\"1\"],\"postIds\":[\"\"]}', null, '1', '不允许操作超级管理员用户', '2021-06-07 19:01:20');
INSERT INTO `sys_oper_log` VALUES ('112', '用户管理', '6', 'com.ruoyi.web.controller.system.SysUserController.importData()', 'POST', '1', 'admin', '研发部门', '/system/user/importData', '127.0.0.1', '内网IP', 'false', '{\r\n  \"msg\" : \"恭喜您，数据已全部导入成功！共 957 条，数据如下：<br/>1、账号 10135065 导入成功<br/>2、账号 13020543 导入成功<br/>3、账号 13020544 导入成功<br/>4、账号 13020546 导入成功<br/>5、账号 13020547 导入成功<br/>6、账号 13020548 导入成功<br/>7、账号 13020549 导入成功<br/>8、账号 13020550 导入成功<br/>9、账号 13020551 导入成功<br/>10、账号 13020552 导入成功<br/>11、账号 13020553 导入成功<br/>12、账号 13020554 导入成功<br/>13、账号 13020555 导入成功<br/>14、账号 13020556 导入成功<br/>15、账号 13020557 导入成功<br/>16、账号 13020558 导入成功<br/>17、账号 10131371 导入成功<br/>18、账号 13020560 导入成功<br/>19、账号 13020561 导入成功<br/>20、账号 13020562 导入成功<br/>21、账号 13020564 导入成功<br/>22、账号 13020565 导入成功<br/>23、账号 13020566 导入成功<br/>24、账号 13020567 导入成功<br/>25、账号 13020568 导入成功<br/>26、账号 13020569 导入成功<br/>27、账号 13020570 导入成功<br/>28、账号 13020572 导入成功<br/>29、账号 13020574 导入成功<br/>30、账号 13020575 导入成功<br/>31、账号 13020577 导入成功<br/>32、账号 13020579 导入成功<br/>33、账号 13020580 导入成功<br/>34、账号 13020581 导入成功<br/>35、账号 13020582 导入成功<br/>36、账号 13020583 导入成功<br/>37、账号 13020584 导入成功<br/>38、账号 13020585 导入成功<br/>39、账号 10120921 导入成功<br/>40、账号 13020588 导入成功<br/>41、账号 13020589 导入成功<br/>42、账号 13020592 导入成功<br/>43、账号 13020593 导入成功<br/>44、账号 13020594 导入成功<br/>45、账号 13020596 导入成功<br/>46、账号 13020597 导入成功<br/>47、账号 13020598 导入成功<br/>48、账号 13020599 导入成功<br/>49、账号 13020601 导入成功<br/>50、账号 13020602 导入成功<br/>51、账号 13020603 导入成功<br/>52、账号 13020605 导入成功<br/>53、账号 13020606 导入成功<br/>54、账号 13020607 导入成功<br/>55、账号 13020608 导入成功<br/>56、账号 13020609 导入成功<br/>57、账号 13020610 导入成功<br/>58、账号 13020611 导入成功<br/>59、账号 13020613 导入成功<br/>60、账号 13020614 导入成功<br/>61、账号 13020615 导入成功<br/>62、账号 13020617 导入成功<br/>63、账号 13020618 导入成功<br/>64、账号 13020620 导入成功<br/>65、账号 13020622 导入成功<br/>66、账号 13020623 导入成功<br/>67、账号 13020624 导入成功<br/>68、账号 13020627 导入成功<br/>69、账号 13020628 导入成功<br/>70、账号 13020629 导入成功<br/>71、账号 13020630 导入成功<br/>72、账号 13020631 导入成功<br/>73、账号 13020632 导入成功<br/>74、账号 13020633 导入成功<br/>75、账号 13020634 导入成功<br/>76、账号 13020635 导入成功<br/>77、账号 13020636 导入成功<br/>78、账号 13020637 导入成功<br/>79、账号 13020639 导入成功<br/>80、账号 13020640 导入成功<br/>81、账号 13020641 导入成功<br/>82、账号 13020643 导入成功', '0', null, '2021-06-07 19:06:22');
INSERT INTO `sys_oper_log` VALUES ('113', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '113.140.3.6', 'XX XX', '{\"userName\":[\"张三\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-06-09 14:54:38');
INSERT INTO `sys_oper_log` VALUES ('114', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '36.46.13.67', 'XX XX', '{\"userName\":[\"赵远东\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1991-10-13\"],\"age\":[\"30\"],\"wokeTime\":[\"2021-06-09\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"610628199110132711\"],\"graduateSchool\":[\"西安工业大学\"],\"graduationMajor\":[\"西安工业大学\"],\"telephone\":[\"17795779907\"],\"address\":[\"西安\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"张女士\"],\"emergencyTelephone\":[\"17795779907\"],\"beforeCorporate\":[\"西安\"],\"beforePost\":[\"延安\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-06-09 16:36:57');
INSERT INTO `sys_oper_log` VALUES ('115', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '36.45.60.69', 'XX XX', '{\"userName\":[\"桂红军\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-06-10 14:59:52');
INSERT INTO `sys_oper_log` VALUES ('116', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '111.18.50.57', 'XX XX', '{\"userName\":[\"附件下载测试\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', null, '1', '上传文件失败', '2021-06-14 10:29:50');
INSERT INTO `sys_oper_log` VALUES ('117', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '111.18.50.57', 'XX XX', '{\"userName\":[\"附件下载测试\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', null, '1', '上传文件失败', '2021-06-14 10:30:18');
INSERT INTO `sys_oper_log` VALUES ('118', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '36.45.60.69', 'XX XX', '{\"userName\":[\"测试数据\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', null, '1', '上传文件失败', '2021-06-14 10:31:49');
INSERT INTO `sys_oper_log` VALUES ('119', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '117.39.224.82', 'XX XX', '{\"userName\":[\"桂红军\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', null, '1', '上传文件失败', '2021-06-16 10:54:47');
INSERT INTO `sys_oper_log` VALUES ('120', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '117.39.224.82', 'XX XX', '{\"userName\":[\"桂红军\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', null, '1', '上传文件失败', '2021-06-16 10:54:51');
INSERT INTO `sys_oper_log` VALUES ('121', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '117.39.224.82', 'XX XX', '{\"userName\":[\"桂红军\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-06-16 15:00:40');
INSERT INTO `sys_oper_log` VALUES ('122', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '113.140.3.6', 'XX XX', '{\"userName\":[\"测试\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-06-24 14:18:54');
INSERT INTO `sys_oper_log` VALUES ('123', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '113.140.3.6', 'XX XX', '{\"userName\":[\"测试数据\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-06-24 14:21:30');
INSERT INTO `sys_oper_log` VALUES ('124', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '36.46.18.110', 'XX XX', '{\"userName\":[\"赵远东\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"2021-06-24\"],\"age\":[\"30\"],\"wokeTime\":[\"2021-06-24\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"610628199110132711\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"机械设计\"],\"telephone\":[\"17795779907\"],\"address\":[\"西安\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"张静\"],\"emergencyTelephone\":[\"15339165152\"],\"beforeCorporate\":[\"西安\"],\"beforePost\":[\"人力资源\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-06-24 14:30:15');
INSERT INTO `sys_oper_log` VALUES ('125', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '117.39.234.253', 'XX XX', '{\"userName\":[\"刘凯鑫\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"1996-08-24\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-04-12\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"612726199608247211\"],\"graduateSchool\":[\"江西科技学院\"],\"graduationMajor\":[\"车辆工程\"],\"telephone\":[\"17386974606\"],\"address\":[\"高陵区泾渭五路\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"刘厚飞\"],\"emergencyTelephone\":[\"15891254377\"],\"beforeCorporate\":[\"中华联合财产保险股份有限公司\"],\"beforePost\":[\"核保\"],\"remark\":[\"无\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-02 08:45:42');
INSERT INTO `sys_oper_log` VALUES ('126', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"测试数据\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115112\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"18092765832\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-02 09:22:14');
INSERT INTO `sys_oper_log` VALUES ('127', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"测试数据\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"2021-07-02\"],\"age\":[\"1\"],\"wokeTime\":[\"2021-07-01\"],\"firstDegree\":[\"硕士\"],\"highestDegree\":[\"研究生\"],\"identityCardNum\":[\"601124199106115115\"],\"graduateSchool\":[\"科大\"],\"graduationMajor\":[\"管理\"],\"telephone\":[\"18092796067\"],\"address\":[\"西安\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"是谁\"],\"emergencyTelephone\":[\"18092794633\"],\"beforeCorporate\":[\"无\"],\"beforePost\":[\"无\"],\"remark\":[\"无\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-02 09:41:14');
INSERT INTO `sys_oper_log` VALUES ('128', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"测试数据0709\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"2021-07-08\"],\"age\":[\"12\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"18092796067\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-09 15:32:32');
INSERT INTO `sys_oper_log` VALUES ('129', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"测数据\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115114\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-09 15:40:19');
INSERT INTO `sys_oper_log` VALUES ('130', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"桂红军\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115113\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-09 15:44:34');
INSERT INTO `sys_oper_log` VALUES ('131', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '113.138.45.227', 'XX XX', '{\"userName\":[\"李超\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"1996-03-01\"],\"age\":[\"25\"],\"wokeTime\":[\"2020-07-01\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"610525199603011331\"],\"graduateSchool\":[\"宝鸡文理学院\"],\"graduationMajor\":[\"机械设计制造及其自动化\"],\"telephone\":[\"15376402125\"],\"address\":[\"陕西省渭南市澄城县冯原镇黎光村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李建斌\"],\"emergencyTelephone\":[\"13474189815\"],\"beforeCorporate\":[\"汉中燎原航空机电工程有限责任公司\"],\"beforePost\":[\"机械加工工艺员\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-16 08:51:00');
INSERT INTO `sys_oper_log` VALUES ('132', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"党哲\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1996-11-28\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-07-21\"],\"firstDegree\":[\"大学本科\"],\"highestDegree\":[\"大学本科\"],\"identityCardNum\":[\"610524199611283218\"],\"graduateSchool\":[\"陕西理工大学\"],\"graduationMajor\":[\"机械设计制造及其自动化\"],\"telephone\":[\"18291327301\"],\"address\":[\"陕西省合阳县解放路北厢中巷11号\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"孙兆侠\"],\"emergencyTelephone\":[\"15229136136\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-21 08:26:34');
INSERT INTO `sys_oper_log` VALUES ('133', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"党哲\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1996-11-28\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-07-21\"],\"firstDegree\":[\"大学本科\"],\"highestDegree\":[\"大学本科\"],\"identityCardNum\":[\"610524199611283218\"],\"graduateSchool\":[\"陕西理工大学\"],\"graduationMajor\":[\"机械设计制造及其自动化\"],\"telephone\":[\"18291327301\"],\"address\":[\"陕西省合阳县解放路北厢中巷11号\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"孙兆侠\"],\"emergencyTelephone\":[\"15229136136\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-21 08:26:40');
INSERT INTO `sys_oper_log` VALUES ('134', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"党哲\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1996-11-28\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-07-21\"],\"firstDegree\":[\"大学本科\"],\"highestDegree\":[\"大学本科\"],\"identityCardNum\":[\"610524199611283218\"],\"graduateSchool\":[\"陕西理工大学\"],\"graduationMajor\":[\"机械设计制造及其自动化\"],\"telephone\":[\"18291327301\"],\"address\":[\"陕西省合阳县解放路北厢中巷11号\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"孙兆侠\"],\"emergencyTelephone\":[\"15229136136\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-21 08:26:44');
INSERT INTO `sys_oper_log` VALUES ('135', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"党哲\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1996-11-28\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-07-21\"],\"firstDegree\":[\"大学本科\"],\"highestDegree\":[\"大学本科\"],\"identityCardNum\":[\"610524199611283218\"],\"graduateSchool\":[\"陕西理工大学\"],\"graduationMajor\":[\"机械设计制造及其自动化\"],\"telephone\":[\"18291327301\"],\"address\":[\"陕西省合阳县解放路北厢中巷11号\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"孙兆侠\"],\"emergencyTelephone\":[\"15229136136\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-21 08:26:52');
INSERT INTO `sys_oper_log` VALUES ('136', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"党哲\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1996-11-28\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-07-21\"],\"firstDegree\":[\"大学本科\"],\"highestDegree\":[\"大学本科\"],\"identityCardNum\":[\"610524199611283218\"],\"graduateSchool\":[\"陕西理工大学\"],\"graduationMajor\":[\"机械设计制造及其自动化\"],\"telephone\":[\"18291327301\"],\"address\":[\"陕西省合阳县解放路北厢中巷11号\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"孙兆侠\"],\"emergencyTelephone\":[\"15229136136\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-21 08:26:52');
INSERT INTO `sys_oper_log` VALUES ('137', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"党哲\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1996-11-28\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-07-21\"],\"firstDegree\":[\"大学本科\"],\"highestDegree\":[\"大学本科\"],\"identityCardNum\":[\"610524199611283218\"],\"graduateSchool\":[\"陕西理工大学\"],\"graduationMajor\":[\"机械设计制造及其自动化\"],\"telephone\":[\"18291327301\"],\"address\":[\"陕西省合阳县解放路北厢中巷11号\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"孙兆侠\"],\"emergencyTelephone\":[\"15229136136\"],\"beforeCorporate\":[\"陕西理工大学\"],\"beforePost\":[\"学生\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-21 08:30:18');
INSERT INTO `sys_oper_log` VALUES ('138', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.139.85.123', 'XX XX', '{\"userName\":[\"赵广耀\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"1996-01-17\"],\"age\":[\"25\"],\"wokeTime\":[\"2014-10-03\"],\"firstDegree\":[\"初中\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610324199601170534\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"18229059694\"],\"address\":[\"陕西省扶风县绛帐镇营中村209号\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"赵旭\"],\"emergencyTelephone\":[\"18629685534\"],\"beforeCorporate\":[\"陕西四方物流公司\"],\"beforePost\":[\"司机\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-21 09:15:14');
INSERT INTO `sys_oper_log` VALUES ('139', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '117.136.87.67', 'XX XX', '{\"userName\":[\"屈换宝\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1991—2—21\"],\"age\":[\"30\"],\"wokeTime\":[\"2021—6—17\"],\"firstDegree\":[\"初中\"],\"highestDegree\":[\"中专\"],\"identityCardNum\":[\"612522199102210319\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"13572044799\"],\"address\":[\"陕西省洛南县卫东镇东湖村二组\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"15829911359\"],\"emergencyTelephone\":[\"15829911359\"],\"beforeCorporate\":[\"上海市宝山区西爱艾电子\"],\"beforePost\":[\"后勤保障\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-22 09:22:06');
INSERT INTO `sys_oper_log` VALUES ('140', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '117.136.87.67', 'XX XX', '{\"userName\":[\"屈换宝\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1991-2-21\"],\"age\":[\"30\"],\"wokeTime\":[\"2021—6—17\"],\"firstDegree\":[\"初中毕业\"],\"highestDegree\":[\"中专\"],\"identityCardNum\":[\"612522199102210319\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"13572044799\"],\"address\":[\"陕西省洛南县卫东镇东湖村二组\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"15829911359\"],\"emergencyTelephone\":[\"15829911359\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-22 09:27:51');
INSERT INTO `sys_oper_log` VALUES ('141', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.11.157', 'XX XX', '{\"userName\":[\"崔怡鹏\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1996-04-20\"],\"age\":[\"25\"],\"wokeTime\":[\"2018-07-01\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"610526199604202814\"],\"graduateSchool\":[\"西安科技大学\"],\"graduationMajor\":[\"安全工程\"],\"telephone\":[\"18791984438\"],\"address\":[\"陕西省蒲城县桥陵镇\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"崔璐\"],\"emergencyTelephone\":[\"18302949121\"],\"beforeCorporate\":[\"湖北省路桥集团有限公司\"],\"beforePost\":[\"安全员\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-27 11:19:24');
INSERT INTO `sys_oper_log` VALUES ('142', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '117.136.86.47', 'XX XX', '{\"userName\":[\"党船\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1997-09-26\"],\"age\":[\"24\"],\"wokeTime\":[\"2020-07-13\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"610524199709260014\"],\"graduateSchool\":[\"西安航空学院\"],\"graduationMajor\":[\"车辆工程\"],\"telephone\":[\"15802967296\"],\"address\":[\"陕西省渭南市合阳县城关街道办大庄头村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"党晓峰\"],\"emergencyTelephone\":[\"13571325962\"],\"beforeCorporate\":[\"三一重能股份有限公司\"],\"beforePost\":[\"服务工程师\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-27 11:24:51');
INSERT INTO `sys_oper_log` VALUES ('143', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '36.46.209.7', 'XX XX', '{\"userName\":[\"姚俊成\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"1998-07-27\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-07-07\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"612732199807270032\"],\"graduateSchool\":[\"燕京理工学院\"],\"graduationMajor\":[\"车辆工程\"],\"telephone\":[\"18812167096\"],\"address\":[\"陕西省榆林市定边县天瑞小区2号楼5号房\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"张亚梅\"],\"emergencyTelephone\":[\"18791841053\"],\"beforeCorporate\":[\"重汽（威海）商用车有限公司\"],\"beforePost\":[\"实习生\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-27 16:46:37');
INSERT INTO `sys_oper_log` VALUES ('144', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '36.46.209.7', 'XX XX', '{\"userName\":[\"姚俊成\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"1998-07-27\"],\"age\":[\"24\"],\"wokeTime\":[\"2021-07-07\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"612732199807270032\"],\"graduateSchool\":[\"燕京理工学院\"],\"graduationMajor\":[\"车辆工程\"],\"telephone\":[\"18812167096\"],\"address\":[\"陕西省榆林市定边县天瑞小区2号楼5号房\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"张亚梅\"],\"emergencyTelephone\":[\"18791841053\"],\"beforeCorporate\":[\"重汽（威海）商用车有限公司\"],\"beforePost\":[\"实习生\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-27 16:52:20');
INSERT INTO `sys_oper_log` VALUES ('145', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"测试数据\"],\"sex\":[\"1\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"160124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-27 18:31:53');
INSERT INTO `sys_oper_log` VALUES ('146', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"GG\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-27 18:35:28');
INSERT INTO `sys_oper_log` VALUES ('147', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"52\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-27 18:38:37');
INSERT INTO `sys_oper_log` VALUES ('148', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '113.143.193.93', 'XX XX', '{\"userName\":[\"何谦\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"2003-08-18\"],\"age\":[\"17\"],\"wokeTime\":[\"2021-03-24\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124200308181239\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"17792372398\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-07-28 00:02:09');
INSERT INTO `sys_oper_log` VALUES ('149', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-02\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                                                           ', '2021-08-02 10:11:51');
INSERT INTO `sys_oper_log` VALUES ('150', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-02\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                                                           ', '2021-08-02 10:11:52');
INSERT INTO `sys_oper_log` VALUES ('151', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-02\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                                                           ', '2021-08-02 10:11:52');
INSERT INTO `sys_oper_log` VALUES ('152', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-02\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                                                           ', '2021-08-02 10:12:02');
INSERT INTO `sys_oper_log` VALUES ('153', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"在上一个单位，干过安全员、技术员和资料员\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6,                          f8 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                              ', '2021-08-02 10:16:43');
INSERT INTO `sys_oper_log` VALUES ('154', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"在上一个单位，干过安全员、技术员和资料员\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6,                          f8 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                              ', '2021-08-02 10:16:48');
INSERT INTO `sys_oper_log` VALUES ('155', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"在上一个单位，干过安全员、技术员和资料员\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6,                          f8 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                              ', '2021-08-02 10:16:54');
INSERT INTO `sys_oper_log` VALUES ('156', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"在上一个单位，干过安全员、技术员和资料员\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6,                          f8 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                              ', '2021-08-02 10:17:00');
INSERT INTO `sys_oper_log` VALUES ('157', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"在上一个单位，干过安全员、技术员和资料员\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6,                          f8 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                              ', '2021-08-02 10:18:47');
INSERT INTO `sys_oper_log` VALUES ('158', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.106', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏油田矿业开发有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"在上一个单位，干过安全员、技术员和资料员\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6,                          f8 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                              ', '2021-08-02 10:20:55');
INSERT INTO `sys_oper_log` VALUES ('159', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '113.140.6.198', 'XX XX', '{\"userName\":[\"测试数据\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"2021-08-02\"],\"age\":[\"12\"],\"wokeTime\":[\"2021-08-03\"],\"firstDegree\":[\"12\"],\"highestDegree\":[\"1\"],\"identityCardNum\":[\"610124199106115112\"],\"graduateSchool\":[\"1\"],\"graduationMajor\":[\"1\"],\"telephone\":[\"18092796068\"],\"address\":[\"1\"],\"isParty\":[\"是\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"12\"],\"emergencyTelephone\":[\"18089765432\"],\"beforeCorporate\":[\"32\"],\"beforePost\":[\"1\"],\"remark\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-08-02 10:43:18');
INSERT INTO `sys_oper_log` VALUES ('160', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏兴油工程技术服务有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"1 、负责施工设计与施工报告的编写、评审、定稿。负责预算和施工任务书的开具以及结算资料的审核；\\r\\n2、负责各类安全、环保法律法规和行业标准的更新及组织学习,负责安全管理规章制度的具体落实和检查监督实行。\\r\\n3、负责环保、消防、安监等月度、年度报表的上报。各类取证、评价、监测等活动的资料准备、现场 陪同等。\\r\\n4、安全生产标准化管理台账的完善。重大危险源的安全管理,各类警告警示安全标识维护\\r\\n5、新入员工的三级安全培训,个人档案的建立;转岗人员的教育培训;承包商人员、临时外协施工人员的安全教育和培训。\\r\\n6、危化品操作、安管人员、特种设备操作(管 理)、消防等相关证件的取证培训安排和落实。\\r\\n7、根据法规要求,参与安全、环保事故预案修订及 备案;并对各类预案、现场处置、突发事件应急处理 等进行演练参与、讨论、评价。\\r\\n8、安全月活动的具体展开。其他安全检查、演练活动的组织开展。\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6,             f7 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                                           ', '2021-08-02 11:04:04');
INSERT INTO `sys_oper_log` VALUES ('161', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏兴油工程技术服务有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"1 、负责施工设计与施工报告的编写、评审、定稿。负责预算和施工任务书的开具以及结算资料的审核；\\r\\n2、负责各类安全、环保法律法规和行业标准的更新及组织学习,负责安全管理规章制度的具体落实和检查监督实行。\\r\\n3、负责环保、消防、安监等月度、年度报表的上报。各类取证、评价、监测等活动的资料准备、现场 陪同等。\\r\\n4、安全生产标准化管理台账的完善。重大危险源的安全管理,各类警告警示安全标识维护\\r\\n5、新入员工的三级安全培训,个人档案的建立;转岗人员的教育培训;承包商人员、临时外协施工人员的安全教育和培训。\\r\\n6、危化品操作、安管人员、特种设备操作(管 理)、消防等相关证件的取证培训安排和落实。\\r\\n7、根据法规要求,参与安全、环保事故预案修订及 备案;并对各类预案、现场处置、突发事件应急处理 等进行演练参与、讨论、评价。\\r\\n8、安全月活动的具体展开。其他安全检查、演练活动的组织开展。\"]}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'f6\' at row 1\r\n### The error may exist in URL [jar:file:/C:/website/wfty-hr-wechat/lib/ruoyi-system-4.6.1.jar!/mapper/system/TEmployeeInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.TEmployeeInfoMapper.insertTEmployeeInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_employee_info          ( user_name,                                                    sex,             nation,                                                                 birth_time,             age,             woke_time,             first_degree,             highest_degree,                                       identity_card_num,                                                    graduate_school,             graduation_major,             telephone,             address,             is_party,             is_marriage,             emergency_name,             emergency_telephone,             before_corporate,             before_post,                                                                                                                                                                                      create_time,                                       remark,                          f1,             f2,             f3,             f4,             f5,             f6,             f7 )           values ( ?,                                                    ?,             ?,                                                                 ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                                                           ', '2021-08-02 11:04:21');
INSERT INTO `sys_oper_log` VALUES ('162', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏兴油工程技术服务有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"1 、负责施工设计与施工报告的编写、评审、定稿。负责预算和施工任务书的开具以及结算资料的审核；\\r\\n2、负责各类安全、环保法律法规和行业标准的更新及组织学习,负责安全管理规章制度的具体落实和检查 监督实行。\\r\\n3、负责环保、消防、安监等月度、年度报表的上报。各类取证、评价、监测等活动的资料准备、现场 陪同等。\\r\\n4、安全生产标准化管理台账的完善。重大危险源的安全管理,各类警告警示安全标识维护\\r\\n5、新入员工的三级安全培训,个人档案的建立;转岗人员的教育培训;承包商人员、临时外协施工人员 的安全教育和培训。\\r\\n6、危化品操作、安管人员、特种设备操作(管 理)、码头、消防等相关证件的取证培训安排和落实。\\r\\n7、根据法规要求,参与安全、环保事故预案修订及 备案;并对各类预案、现场处置、突发事件应急处理 等进行演练参与、讨论、评价。\\r\\n8、安全月活动的具体展开。其他安全检查、演练活动的组织开展。\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-08-02 11:14:54');
INSERT INTO `sys_oper_log` VALUES ('163', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.111', 'XX XX', '{\"userName\":[\"张晓兵\"],\"sex\":[\"0\"],\"nation\":[\"汉\"],\"birthTime\":[\"1993-06-02\"],\"age\":[\"28\"],\"wokeTime\":[\"2018-07-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"620522199306023339\"],\"graduateSchool\":[\"兰州城市学院\"],\"graduationMajor\":[\"油气储运工程\"],\"telephone\":[\"18893493008\"],\"address\":[\"甘肃省秦安县魏店镇张坡村\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"李红娟\"],\"emergencyTelephone\":[\"18894491760\"],\"beforeCorporate\":[\"江苏兴油工程技术服务有限公司\"],\"beforePost\":[\"技术员\"],\"remark\":[\"1 、负责施工设计与施工报告的编写、评审、定稿。负责预算和施工任务书的开具以及结算资料的审核；\\r\\n2、负责各类安全、环保法律法规和行业标准的更新及组织学习,负责安全管理规章制度的具体落实和检查 监督实行。\\r\\n3、负责环保、消防、安监等月度、年度报表的上报。各类取证、评价、监测等活动的资料准备、现场 陪同等。\\r\\n4、安全生产标准化管理台账的完善。重大危险源的安全管理,各类警告警示安全标识维护\\r\\n5、新入员工的三级安全培训,个人档案的建立;转岗人员的教育培训;承包商人员、临时外协施工人员 的安全教育和培训。\\r\\n6、危化品操作、安管人员、特种设备操作(管 理)、码头、消防等相关证件的取证培训安排和落实。\\r\\n7、根据法规要求,参与安全、环保事故预案修订及 备案;并对各类预案、现场处置、突发事件应急处理 等进行演练参与、讨论、评价。\\r\\n8、安全月活动的具体展开。其他安全检查、演练活动的组织开展。\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-08-02 11:18:35');
INSERT INTO `sys_oper_log` VALUES ('164', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.46', 'XX XX', '{\"userName\":[\"苏桐玉\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"1999-12-08\"],\"age\":[\"22\"],\"wokeTime\":[\"2021-08-23\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"610424199912087637\"],\"graduateSchool\":[\"西安交通大学城市学院\"],\"graduationMajor\":[\"机械设计制造及其自动化\"],\"telephone\":[\"18329756183\"],\"address\":[\"陕西省咸阳市乾县钱塘锦绣\"],\"isParty\":[\"否\"],\"isMarriage\":[\"否\"],\"emergencyName\":[\"苏印\"],\"emergencyTelephone\":[\"18329959386\"],\"beforeCorporate\":[\"宁波上中下自动变速器厂\"],\"beforePost\":[\"机加操作工\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-08-19 09:16:13');
INSERT INTO `sys_oper_log` VALUES ('165', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '223.104.204.46', 'XX XX', '{\"userName\":[\"苏桐玉\"],\"sex\":[\"0\"],\"nation\":[\"\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610424199912087637\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-08-19 10:36:01');
INSERT INTO `sys_oper_log` VALUES ('166', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"测试数据1\"],\"sex\":[\"0\"],\"nation\":[\"e\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115115\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-08-19 18:57:47');
INSERT INTO `sys_oper_log` VALUES ('167', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '123.138.87.9', 'XX XX', '{\"userName\":[\"测试数据2\"],\"sex\":[\"0\"],\"nation\":[\"e\"],\"birthTime\":[\"\"],\"age\":[\"\"],\"wokeTime\":[\"\"],\"firstDegree\":[\"\"],\"highestDegree\":[\"\"],\"identityCardNum\":[\"610124199106115119\"],\"graduateSchool\":[\"\"],\"graduationMajor\":[\"\"],\"telephone\":[\"\"],\"address\":[\"\"],\"isParty\":[\"是\"],\"isMarriage\":[\"婚\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-08-19 19:00:46');
INSERT INTO `sys_oper_log` VALUES ('168', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', '1', 'admin', null, '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"人员管理\"],\"url\":[\"/system/info\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"10\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-01 15:48:04');
INSERT INTO `sys_oper_log` VALUES ('169', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'GET', '1', 'admin', null, '/system/menu/remove/1062', '127.0.0.1', '内网IP', '1062', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-01 15:48:13');
INSERT INTO `sys_oper_log` VALUES ('170', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', '1', 'admin', null, '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1\"],\"menuType\":[\"C\"],\"menuName\":[\"人员管理\"],\"url\":[\"/system/info\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"10\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-01 15:48:41');
INSERT INTO `sys_oper_log` VALUES ('171', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.editSave()', 'POST', '1', 'admin', null, '/system/notice/edit', '127.0.0.1', '内网IP', '{\"noticeId\":[\"4\"],\"noticeTitle\":[\"通知\"],\"noticeType\":[\"1\"],\"noticeContent\":[\"党哲个人信息没问题\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-02 09:48:49');
INSERT INTO `sys_oper_log` VALUES ('172', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', 'admin', null, '/system/info/add', '127.0.0.1', '内网IP', '{\"userName\":[\"滕一帆\"],\"sex\":[\"0\"],\"nation\":[\"苗\"],\"birthTime\":[\"1997-02-14\"],\"age\":[\"25\"],\"wokeTime\":[\"2022-08-02\"],\"firstDegree\":[\"高中以下\"],\"highestDegree\":[\"高中\"],\"identityCardNum\":[\"433123199702140037\"],\"graduateSchool\":[\"1\"],\"graduationMajor\":[\"1\"],\"school\":[\"1\"],\"major\":[\"1\"],\"telephone\":[\"18161910796\"],\"address\":[\"1\"],\"isParty\":[\"Y\"],\"isMarriage\":[\"Y\"],\"emergencyName\":[\"1\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-02 10:25:40');
INSERT INTO `sys_oper_log` VALUES ('173', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '61.185.195.198', 'XX XX', '{\"userName\":[\"滕一帆\"],\"sex\":[\"0\"],\"nation\":[\"苗\"],\"birthTime\":[\"2022-08-03\"],\"age\":[\"25\"],\"wokeTime\":[\"2022-08-03\"],\"firstDegree\":[\"高中以下\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"433123199702140037\"],\"graduateSchool\":[\"25\"],\"graduationMajor\":[\"356\"],\"school\":[\"\"],\"major\":[\"\"],\"telephone\":[\"18161910796\"],\"address\":[\"西安\"],\"isParty\":[\"Y\"],\"isMarriage\":[\"Y\"],\"emergencyName\":[\"一帆\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-03 19:52:00');
INSERT INTO `sys_oper_log` VALUES ('174', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '61.185.195.198', 'XX XX', '{\"userName\":[\"滕一帆\"],\"sex\":[\"0\"],\"nation\":[\"苗\"],\"birthTime\":[\"2022-08-03\"],\"age\":[\"25\"],\"wokeTime\":[\"2022-08-03\"],\"firstDegree\":[\"大专\"],\"highestDegree\":[\"硕士\"],\"identityCardNum\":[\"433123199702140037\"],\"graduateSchool\":[\"123456\"],\"graduationMajor\":[\"123456\"],\"school\":[\"\"],\"major\":[\"\"],\"telephone\":[\"18161910796\"],\"address\":[\"西安\"],\"isParty\":[\"Y\"],\"isMarriage\":[\"Y\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-03 19:54:53');
INSERT INTO `sys_oper_log` VALUES ('175', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '61.185.195.198', 'XX XX', '{\"userName\":[\"滕一帆\"],\"sex\":[\"0\"],\"nation\":[\"苗\"],\"birthTime\":[\"2022-08-03\"],\"age\":[\"25\"],\"wokeTime\":[\"2022-08-03\"],\"firstDegree\":[\"本科\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"433123199702140037\"],\"graduateSchool\":[\"65656\"],\"graduationMajor\":[\"522\"],\"school\":[\"22555\"],\"major\":[\"\"],\"telephone\":[\"18161910799\"],\"address\":[\"西安\"],\"isParty\":[\"Y\"],\"isMarriage\":[\"Y\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-03 20:00:46');
INSERT INTO `sys_oper_log` VALUES ('176', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '61.185.195.198', 'XX XX', '{\"userName\":[\"滕一帆\"],\"sex\":[\"0\"],\"nation\":[\"苗\"],\"birthTime\":[\"2022-08-03\"],\"age\":[\"25\"],\"wokeTime\":[\"2022-08-03\"],\"firstDegree\":[\"高中\"],\"highestDegree\":[\"本科\"],\"identityCardNum\":[\"433123199702140037\"],\"graduateSchool\":[\"师范大学\"],\"graduationMajor\":[\"本科\"],\"school\":[\"\"],\"openid\":[\"oxz-d6boZXOIGa6m8ZCE9hhhkFUY\"],\"major\":[\"\"],\"telephone\":[\"18161910796\"],\"address\":[\"西安\"],\"isParty\":[\"Y\"],\"isMarriage\":[\"Y\"],\"emergencyName\":[\"\"],\"emergencyTelephone\":[\"\"],\"beforeCorporate\":[\"\"],\"beforePost\":[\"\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-03 20:04:24');
INSERT INTO `sys_oper_log` VALUES ('177', '员工信息', '1', 'com.ruoyi.system.controller.TEmployeeInfoController.addSave()', 'POST', '1', null, null, '/system/info/add', '117.22.216.254', 'XX XX', '{\"userName\":[\"曹操\"],\"sex\":[\"0\"],\"nation\":[\"汉族\"],\"birthTime\":[\"1899-12-31\"],\"age\":[\"45\"],\"wokeTime\":[\"2022-08-06\"],\"firstDegree\":[\"高中\"],\"highestDegree\":[\"高中\"],\"identityCardNum\":[\"610121020010034352\"],\"graduateSchool\":[\"1\"],\"graduationMajor\":[\"2\"],\"school\":[\"3\"],\"openid\":[\"oxz-d6boZXOIGa6m8ZCE9hhhkFUY\"],\"major\":[\"4\"],\"telephone\":[\"13152689531\"],\"address\":[\"4\"],\"isParty\":[\"Y\"],\"isMarriage\":[\"Y\"],\"emergencyName\":[\"5\"],\"emergencyTelephone\":[\"13152689531\"],\"beforeCorporate\":[\"5\"],\"beforePost\":[\"5\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2022-08-06 19:47:55');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(11) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(11) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'admin', '1', '1', '0', '0', 'admin', '2021-06-07 18:52:13', '', null, '超级管理员');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '4');
INSERT INTO `sys_role_menu` VALUES ('2', '100');
INSERT INTO `sys_role_menu` VALUES ('2', '101');
INSERT INTO `sys_role_menu` VALUES ('2', '102');
INSERT INTO `sys_role_menu` VALUES ('2', '103');
INSERT INTO `sys_role_menu` VALUES ('2', '104');
INSERT INTO `sys_role_menu` VALUES ('2', '105');
INSERT INTO `sys_role_menu` VALUES ('2', '106');
INSERT INTO `sys_role_menu` VALUES ('2', '107');
INSERT INTO `sys_role_menu` VALUES ('2', '108');
INSERT INTO `sys_role_menu` VALUES ('2', '109');
INSERT INTO `sys_role_menu` VALUES ('2', '110');
INSERT INTO `sys_role_menu` VALUES ('2', '111');
INSERT INTO `sys_role_menu` VALUES ('2', '112');
INSERT INTO `sys_role_menu` VALUES ('2', '113');
INSERT INTO `sys_role_menu` VALUES ('2', '114');
INSERT INTO `sys_role_menu` VALUES ('2', '115');
INSERT INTO `sys_role_menu` VALUES ('2', '116');
INSERT INTO `sys_role_menu` VALUES ('2', '500');
INSERT INTO `sys_role_menu` VALUES ('2', '501');
INSERT INTO `sys_role_menu` VALUES ('2', '1000');
INSERT INTO `sys_role_menu` VALUES ('2', '1001');
INSERT INTO `sys_role_menu` VALUES ('2', '1002');
INSERT INTO `sys_role_menu` VALUES ('2', '1003');
INSERT INTO `sys_role_menu` VALUES ('2', '1004');
INSERT INTO `sys_role_menu` VALUES ('2', '1005');
INSERT INTO `sys_role_menu` VALUES ('2', '1006');
INSERT INTO `sys_role_menu` VALUES ('2', '1007');
INSERT INTO `sys_role_menu` VALUES ('2', '1008');
INSERT INTO `sys_role_menu` VALUES ('2', '1009');
INSERT INTO `sys_role_menu` VALUES ('2', '1010');
INSERT INTO `sys_role_menu` VALUES ('2', '1011');
INSERT INTO `sys_role_menu` VALUES ('2', '1012');
INSERT INTO `sys_role_menu` VALUES ('2', '1013');
INSERT INTO `sys_role_menu` VALUES ('2', '1014');
INSERT INTO `sys_role_menu` VALUES ('2', '1015');
INSERT INTO `sys_role_menu` VALUES ('2', '1016');
INSERT INTO `sys_role_menu` VALUES ('2', '1017');
INSERT INTO `sys_role_menu` VALUES ('2', '1018');
INSERT INTO `sys_role_menu` VALUES ('2', '1019');
INSERT INTO `sys_role_menu` VALUES ('2', '1020');
INSERT INTO `sys_role_menu` VALUES ('2', '1021');
INSERT INTO `sys_role_menu` VALUES ('2', '1022');
INSERT INTO `sys_role_menu` VALUES ('2', '1023');
INSERT INTO `sys_role_menu` VALUES ('2', '1024');
INSERT INTO `sys_role_menu` VALUES ('2', '1025');
INSERT INTO `sys_role_menu` VALUES ('2', '1026');
INSERT INTO `sys_role_menu` VALUES ('2', '1027');
INSERT INTO `sys_role_menu` VALUES ('2', '1028');
INSERT INTO `sys_role_menu` VALUES ('2', '1029');
INSERT INTO `sys_role_menu` VALUES ('2', '1030');
INSERT INTO `sys_role_menu` VALUES ('2', '1031');
INSERT INTO `sys_role_menu` VALUES ('2', '1032');
INSERT INTO `sys_role_menu` VALUES ('2', '1033');
INSERT INTO `sys_role_menu` VALUES ('2', '1034');
INSERT INTO `sys_role_menu` VALUES ('2', '1035');
INSERT INTO `sys_role_menu` VALUES ('2', '1036');
INSERT INTO `sys_role_menu` VALUES ('2', '1037');
INSERT INTO `sys_role_menu` VALUES ('2', '1038');
INSERT INTO `sys_role_menu` VALUES ('2', '1039');
INSERT INTO `sys_role_menu` VALUES ('2', '1040');
INSERT INTO `sys_role_menu` VALUES ('2', '1041');
INSERT INTO `sys_role_menu` VALUES ('2', '1042');
INSERT INTO `sys_role_menu` VALUES ('2', '1043');
INSERT INTO `sys_role_menu` VALUES ('2', '1044');
INSERT INTO `sys_role_menu` VALUES ('2', '1045');
INSERT INTO `sys_role_menu` VALUES ('2', '1046');
INSERT INTO `sys_role_menu` VALUES ('2', '1047');
INSERT INTO `sys_role_menu` VALUES ('2', '1048');
INSERT INTO `sys_role_menu` VALUES ('2', '1049');
INSERT INTO `sys_role_menu` VALUES ('2', '1050');
INSERT INTO `sys_role_menu` VALUES ('2', '1051');
INSERT INTO `sys_role_menu` VALUES ('2', '1052');
INSERT INTO `sys_role_menu` VALUES ('2', '1053');
INSERT INTO `sys_role_menu` VALUES ('2', '1054');
INSERT INTO `sys_role_menu` VALUES ('2', '1055');
INSERT INTO `sys_role_menu` VALUES ('2', '1056');
INSERT INTO `sys_role_menu` VALUES ('2', '1057');
INSERT INTO `sys_role_menu` VALUES ('2', '1058');
INSERT INTO `sys_role_menu` VALUES ('2', '1059');
INSERT INTO `sys_role_menu` VALUES ('2', '1060');
INSERT INTO `sys_role_menu` VALUES ('2', '1061');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2015 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', null, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2022-08-03 18:12:31', '2022-01-24 20:33:43', 'admin', '2022-01-24 20:33:43', '', '2022-08-03 18:12:31', '管理员');

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(11) DEFAULT '0' COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='在线用户记录';

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');

-- ----------------------------
-- Table structure for t_employee_info
-- ----------------------------
DROP TABLE IF EXISTS `t_employee_info`;
CREATE TABLE `t_employee_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL COMMENT '员工编号',
  `dept_code` bigint(20) DEFAULT NULL COMMENT '组织编码',
  `post_code` bigint(20) DEFAULT NULL COMMENT '岗位编码',
  `login_name` varchar(30) DEFAULT NULL COMMENT '账号',
  `user_name` varchar(30) NOT NULL DEFAULT '' COMMENT '姓名',
  `user_type` varchar(2) DEFAULT '00' COMMENT '类型（00系统用户 01注册用户）',
  `insurance_num` varchar(36) DEFAULT NULL COMMENT '保险编号',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `nation` varchar(36) DEFAULT NULL COMMENT '民族',
  `contract_time` date DEFAULT NULL COMMENT '合同时间',
  `stop_time` date DEFAULT NULL COMMENT '终止时间',
  `entry_time` date DEFAULT NULL COMMENT '进厂时间',
  `work_age` double DEFAULT NULL COMMENT '工龄',
  `birth_time` date DEFAULT NULL COMMENT '出生时间',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `woke_time` date DEFAULT NULL COMMENT '工作时间',
  `first_degree` varchar(18) DEFAULT NULL COMMENT '第一学历',
  `highest_degree` varchar(18) DEFAULT NULL COMMENT '最高学历',
  `post_id` bigint(20) DEFAULT NULL COMMENT '岗位编码',
  `identity_num` varchar(20) DEFAULT NULL COMMENT '身份编号',
  `identity_card_num` varchar(20) DEFAULT NULL COMMENT '身份证号码',
  `title_seq` varchar(20) DEFAULT NULL COMMENT '职称序列',
  `title_level` varchar(8) DEFAULT NULL COMMENT '职称级别',
  `title_name` varchar(50) DEFAULT NULL COMMENT '职称名称',
  `graduate_school` varchar(50) DEFAULT NULL COMMENT '毕业院校',
  `graduation_major` varchar(50) DEFAULT NULL COMMENT '毕业专业',
  `telephone` varchar(12) DEFAULT NULL COMMENT '电话',
  `address` varchar(100) DEFAULT NULL COMMENT '家庭住址',
  `is_party` varchar(1) DEFAULT NULL COMMENT '是否党员',
  `is_marriage` varchar(1) DEFAULT NULL COMMENT '婚否',
  `emergency_name` varchar(36) DEFAULT NULL COMMENT '紧急联系人姓名',
  `emergency_telephone` varchar(12) DEFAULT NULL COMMENT '紧急联系电话',
  `before_corporate` varchar(50) DEFAULT NULL COMMENT '原工作单位名称',
  `before_post` varchar(50) DEFAULT NULL COMMENT '原工作岗位',
  `column_one` varchar(64) DEFAULT NULL COMMENT '预留字段1',
  `column_two` varchar(36) DEFAULT NULL COMMENT '预留字段2',
  `column_three` bigint(20) DEFAULT NULL COMMENT '预留字段三',
  `column_four` double DEFAULT NULL COMMENT '预留字段4',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '2' COMMENT '备注',
  `openid` varchar(1024) DEFAULT NULL COMMENT '微信openid',
  `access_token` varchar(255) DEFAULT NULL COMMENT 'access_token',
  `f1` varchar(1024) DEFAULT NULL COMMENT '附件1',
  `f2` varchar(1024) DEFAULT NULL COMMENT '附件2',
  `f3` varchar(1024) DEFAULT NULL COMMENT '附件3',
  `f4` varchar(1024) DEFAULT NULL COMMENT '附件4',
  `f5` varchar(1024) DEFAULT NULL COMMENT '附件5',
  `f6` varchar(1024) DEFAULT NULL COMMENT '附件6',
  `f7` varchar(1024) DEFAULT NULL COMMENT '附件7',
  `f8` varchar(1024) DEFAULT NULL COMMENT '附件8',
  `school` varchar(255) DEFAULT NULL COMMENT '学校',
  `major` varchar(255) DEFAULT NULL COMMENT 'major',
  `approval_status` varchar(125) DEFAULT NULL COMMENT '审核状态',
  `approval_idea` varchar(255) DEFAULT NULL COMMENT '审核原因',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='员工信息表';

-- ----------------------------
-- Records of t_employee_info
-- ----------------------------
INSERT INTO `t_employee_info` VALUES ('30', null, null, null, null, '曹操', '00', null, '0', '汉族', null, null, null, null, '1899-12-31', '45', '2022-08-06', '高中', '高中', null, null, '610121020010034352', null, null, null, '1', '2', '13152689531', '4', 'Y', 'Y', '5', '13152689531', '5', '5', null, null, null, null, '', '', '', '0', '0', '', null, null, '', '2022-08-06 19:47:55', '', null, '', 'oxz-d6boZXOIGa6m8ZCE9hhhkFUY', null, null, null, null, null, null, null, null, null, '3', '4', null, null);

-- ----------------------------
-- Table structure for t_salary_info
-- ----------------------------
DROP TABLE IF EXISTS `t_salary_info`;
CREATE TABLE `t_salary_info` (
  `salary_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `salary_month` varchar(64) DEFAULT '""' COMMENT '月份',
  `safe_num` varchar(125) DEFAULT '""' COMMENT '保险编号',
  `emplayee_id` varchar(125) DEFAULT '""' COMMENT '员工编号',
  `emplayee_name` varchar(64) DEFAULT '""' COMMENT '员工姓名',
  `salary_type` int(11) DEFAULT '0' COMMENT '工资类型',
  `into_job_date` varchar(64) DEFAULT '""' COMMENT '入职日期',
  `out_job_date` varchar(64) DEFAULT '""' COMMENT '离职日期',
  `emplayee_dept` varchar(64) DEFAULT '""' COMMENT '部门',
  `emplayee_dept_owner` varchar(64) DEFAULT '""' COMMENT '所属部门',
  `salary_base_num` decimal(15,2) DEFAULT '0.00' COMMENT '基本工资',
  `piecework_hours` decimal(15,2) DEFAULT '0.00' COMMENT '计件工时',
  `time_unit_price` decimal(15,2) DEFAULT '0.00' COMMENT '工时单价',
  `price_rate_one` decimal(15,2) DEFAULT '0.00' COMMENT '计件工资40%',
  `cadre_assessment_coefficient` decimal(15,2) DEFAULT '0.00' COMMENT '干部考核系数',
  `price_rate_two` decimal(15,2) DEFAULT '0.00' COMMENT '计件工资60%',
  `price_rate` decimal(15,2) DEFAULT '0.00' COMMENT '计件工资',
  `performance_rating` varchar(125) DEFAULT NULL COMMENT '绩效等级',
  `performance_coefficient` decimal(15,2) DEFAULT '0.00' COMMENT '绩效系数',
  `performance_rate` decimal(15,2) DEFAULT '0.00' COMMENT '绩效工资',
  `point_wages` decimal(15,2) DEFAULT '0.00' COMMENT '薪点工资',
  `vehicles_subsidies` decimal(15,2) DEFAULT '0.00' COMMENT '车辆补贴',
  `meal_card_subsidies` decimal(15,2) DEFAULT '0.00' COMMENT '饭卡补助',
  `work_age_rate` decimal(15,2) DEFAULT '0.00' COMMENT '工龄工资',
  `one_child_rate` decimal(15,2) DEFAULT '0.00' COMMENT '独子费',
  `no_friend_rate` decimal(15,2) DEFAULT '0.00' COMMENT '单身补助',
  `post_allowance` decimal(15,2) DEFAULT '0.00' COMMENT '职务津贴',
  `honor_allowance` decimal(15,2) DEFAULT '0.00' COMMENT '荣誉津贴',
  `night_shift_allowance` decimal(15,2) DEFAULT '0.00' COMMENT '夜班津贴',
  `skills_allowance` decimal(15,2) DEFAULT '0.00' COMMENT '技术技能津贴',
  `department_award` decimal(15,2) DEFAULT '0.00' COMMENT '部门奖励',
  `quality_award` decimal(15,2) DEFAULT '0.00' COMMENT '质量奖励',
  `on_duty_fee` decimal(15,2) DEFAULT '0.00' COMMENT '值班费',
  `pioneer_award` decimal(15,2) DEFAULT '0.00' COMMENT '先锋奖励',
  `rationalization_reward` decimal(15,2) DEFAULT '0.00' COMMENT '合理化奖励',
  `transfer_wages` decimal(15,2) DEFAULT '0.00' COMMENT '划拨工资',
  `other_award` decimal(15,2) DEFAULT '0.00' COMMENT '其他奖励',
  `heating_rate` decimal(15,2) DEFAULT '0.00' COMMENT '取暖费',
  `cool_fee` decimal(15,2) DEFAULT '0.00' COMMENT '降温费',
  `height_temperature_allowance` decimal(15,2) DEFAULT '0.00' COMMENT '高温津贴',
  `retroactive_pay` decimal(15,2) DEFAULT '0.00' COMMENT '补发工资',
  `attendance_fine` decimal(15,2) DEFAULT '0.00' COMMENT '考勤罚款',
  `fine_quality` decimal(15,2) DEFAULT '0.00' COMMENT '质量罚款',
  `other_fine` decimal(15,2) DEFAULT '0.00' COMMENT '其他罚款',
  `fill_buckle` decimal(15,2) DEFAULT '0.00' COMMENT '补扣',
  `punish_salary` decimal(15,2) DEFAULT '0.00' COMMENT '扣罚工资',
  `other_salary` decimal(15,2) DEFAULT '0.00' COMMENT '其他',
  `should_pay` decimal(15,2) DEFAULT '0.00' COMMENT '应发工资',
  `pension` decimal(15,2) DEFAULT '0.00' COMMENT '养老金',
  `accumulation_fund` decimal(15,2) DEFAULT '0.00' COMMENT '公积金',
  `health_care` decimal(15,2) DEFAULT '0.00' COMMENT '医保',
  `unemployment_benefits` decimal(15,2) DEFAULT '0.00' COMMENT '失业金',
  `illness_assistance` decimal(15,2) DEFAULT '0.00' COMMENT '大病互助',
  `annuity` decimal(15,2) DEFAULT '0.00' COMMENT '年金',
  `provident_subsidy` decimal(15,2) DEFAULT '0.00' COMMENT '公积金补扣',
  `enterprise_annuity` decimal(15,2) DEFAULT '0.00' COMMENT '企业年金',
  `dormitory_fee` decimal(15,2) DEFAULT '0.00' COMMENT '宿舍费',
  `casualty` decimal(15,2) DEFAULT '0.00' COMMENT '意外险',
  `special_deduction` decimal(15,2) DEFAULT '0.00' COMMENT '专项扣除',
  `plan_duty_salary_three` decimal(15,2) DEFAULT '0.00' COMMENT '计税工资3',
  `plan_duty_salary_two` decimal(15,2) DEFAULT '0.00' COMMENT '计税工资2',
  `plan_duty_salary_one` decimal(15,2) DEFAULT '0.00' COMMENT '计税工资1',
  `cumulative_taxable_wages` decimal(15,2) DEFAULT '0.00' COMMENT '累积计税工资',
  `income_tax_three` decimal(15,2) DEFAULT '0.00' COMMENT '所得税3',
  `income_tax_two` decimal(15,2) DEFAULT '0.00' COMMENT '所得税2',
  `income_tax_one` decimal(15,2) DEFAULT '0.00' COMMENT '所得税1',
  `moneth_income_tax` decimal(15,2) DEFAULT '0.00' COMMENT '本月应缴所得税',
  `withholding_wages` decimal(15,2) DEFAULT '0.00' COMMENT '代扣工资',
  `real_wages` decimal(15,2) DEFAULT '0.00' COMMENT '实发工资',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(125) DEFAULT NULL COMMENT '备注',
  `a1` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a2` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a3` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a4` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a5` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a6` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a7` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a8` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a9` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a10` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a11` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a12` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a13` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a14` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a15` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a16` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a17` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a18` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a19` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a20` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a21` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a22` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a23` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a24` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a25` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a26` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a27` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a28` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a29` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  `a30` decimal(15,2) DEFAULT '0.00' COMMENT '备用1',
  PRIMARY KEY (`salary_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2801 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='工资信息表';

-- ----------------------------
-- Records of t_salary_info
-- ----------------------------
